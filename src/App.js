import logo from './logo.svg';
import './App.css';
import MyNavBar from './components/MyNavBar';
import 'bootstrap/dist/css/bootstrap.min.css'
import MyCard from './components/MyCard';

function App() {
  let products = [
    {
      name: "iPhone 12 Pro Max",
    },
    {
      name: "iPhone 12 Pro",
    },
    {
      name: "iPhone 12",
    }, {
      name: "iPhone 12",
    },
    {
      name: "iPhone 12",
    },
    {
      name: "iPhone 12",
    },

  ]
  return (
   <>
   <MyNavBar/>
   <div className="crad-wrapper container mt-3 mb-3">
    <div className="row">
      {
        products.map((product)=>(
          <div className='col-4 d-flex justify-conten-center'> 
              <MyCard/>
          </div>
        ))
      }
    </div>

   </div>
   </>
   
  );
}

export default App;
